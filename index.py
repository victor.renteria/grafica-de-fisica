import matplotlib.pyplot as plt

with open("./Espectro_radiacion.txt", "r")  as f:
    iterator=0
    longitudDeOnda=[]
    intensidad=[]
    for linea in f:
        if iterator!=0:
            longitudDeOnda.append(float(linea.split()[0]))
            intensidad.append(float(linea.split()[1]))
        iterator+=1
    plt.xlabel('Longitud de onda [nm]')
    plt.ylabel('Intensidad [W/m2]')
    plt.title('Espectro de radiacion')
    plt.plot(longitudDeOnda,intensidad)
    plt.grid(True, linestyle='-.')
    plt.tick_params(labelcolor='r', labelsize='medium', width=3)
    plt.show()
